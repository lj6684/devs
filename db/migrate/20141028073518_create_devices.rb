class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :ip
      t.string :os_type
      t.string :os_name
      t.string :os_bit
      t.string :browser_version
      t.string :browser_bit
      t.string :owner
      t.integer :status
      t.string :description
      t.text :remark      

      t.timestamps
    end
  end
end
