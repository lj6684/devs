# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Device.delete_all

Device.create(
  [{
      ip: '172.16.13.11',
      os_type: 'Windows',
      os_name: 'Windows7',
      os_bit: '64-bit',
      browser_version: 'ie8',
      browser_bit: '32-bit',
      owner: 'lijian',
      description: 'development enviroment',
      status: 0
    },
    {
      ip: '172.16.13.12',
      os_type: 'Windows',
      os_name: 'Windows8',
      os_bit: '64-bit',
      browser_version: 'ie10',
      browser_bit: '32-bit',
      owner: 'ZYK',
      description: 'development enviroment',
      status: 1
    },
    {
      ip: '172.16.13.13',
      os_type: 'Windows',
      os_name: 'WindowsXP',
      os_bit: '32-bit',
      browser_version: 'ie7',
      browser_bit: '32-bit',
      owner: 'ZYK',
      description: 'development enviroment',
      status: 0
    }]
)